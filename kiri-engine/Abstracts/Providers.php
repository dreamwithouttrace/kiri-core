<?php
declare(strict_types=1);

namespace Kiri\Abstracts;


/**
 * Class Providers
 * @package Kiri\Abstracts
 */
abstract class Providers extends Component implements Provider
{

}
