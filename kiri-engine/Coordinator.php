<?php
declare(strict_types=1);


namespace Kiri;

class Coordinator
{

	const string WORKER_START = 'worker:start';

	private bool $waite = true;


	/**
	 * @return void
	 */
	public function yield(): void
	{
		while ($this->waite) {
            usleep(1000);
        }
	}


	/**
	 * @return void
	 */
	public function waite(): void
	{
		$this->waite = true;
	}


	/**
	 * @return void
	 */
	public function done(): void
	{
		$this->waite = false;
	}


}
