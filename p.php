<?php


use Swoole\Timer;
use Swoole\WebSocket\Server;

$client = new Server("0.0.0.0", 9511);
$client->addProcess(new Swoole\Process(function (\Swoole\Process $server) use ($client) {
    while (true) {
        echo json_encode($client->stats(), JSON_UNESCAPED_UNICODE) . PHP_EOL;
        sleep(1);
    }
}));
$client->on('open', function (Server $server) {

});

$client->on('message', function (Server $server, $frame) {

});

$client->on('close', function (Server $server, $fd) {

});
$client->start();